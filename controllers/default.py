# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
#########################################################################
from datetime import datetime
from datetime import time
from datetime import date

def index():
    calendar_list = {}
    if auth.user_id is not None:
        calendar_list = db((db.calendar.author == auth.user_id)).select(orderby=~db.calendar.created_on)
    return dict(calendar_list = calendar_list)

def show_calendars():
    calendar_list = {}
    if auth.user_id is not None:
        calendar_list = db((db.calendar.author == auth.user_id)).select()
        d= {r.id: {'calendar_name': r.name,
                'calendar_uid':r.id}
        for r in calendar_list}
    return response.json(dict(calendar_list=d))

@auth.requires_login()
def add_calendar():
    form = SQLFORM(db.calendar, fields=['name', 'start_date', 'end_date'])
    #Possible future update:
    #db.board.update_or_insert((db.calendar.id == request.vars.calendar_id),
    #                            name=request.vars.name,
    #                            start_date=request.vars.start_date,
    #                            end_date=request.vars.end_date,
    #                            is_favorite=request.vars.is_favorite,
    #                          )
    if form.process().accepted:
        session.flash = T('New board created')
        redirect(URL('index'))
    return dict(form = form)


@auth.requires_login()
def duplicate_calendar():
    calendarid=request.args(0)
    calendar = db(db.calendar.id == calendarid).select().first()
    start = db.calendar(calendarid).start_date
    end = db.calendar(calendarid).end_date

    name = 'Duplicate '
    i = 0
    while True:
        calend = db(db.calendar.name == name).select().first()
        if calend is not None:
            i=int(i+1)
            name = 'Duplicate '
            name = name + str(i)
        else:
            break

    db.calendar.insert(name= name, start_date=start, end_date=end)
    c = (db(db.calendar.name == name).select()).first()

    event_list = db(db.calendar_event.calendar == calendar.id).select()
    if event_list is not None:
        for e in event_list:
            db.calendar_event.insert(calendar = c.id, title=e.title,author=e.author, date_occur=e.date_occur,description=e.description,recur=e.recur, recurring=e.recurring,start_time=e.start_time,end_time=e.end_time)
    redirect(URL('default', 'index'))
    return "okay"

@auth.requires_login()
def merge_calendars():
    #calendar_ids= request.vars.get('marked_calendars[]')
    calendar_ids=request.vars.get('marked_calendars[]', [])
    if isinstance(calendar_ids, (str, unicode)):
	    calendar_ids = [calendar_ids]
    earliest_start = db.calendar(calendar_ids[0]).start_date
    latest_end = db.calendar(calendar_ids[0]).end_date

    for c in calendar_ids:
        calendar_start = db.calendar(c).start_date
        calendar_end = db.calendar(c).end_date
        if(calendar_start < earliest_start):
            earliest_start = calendar_start
        if(calendar_end > latest_end):
            latest_end = calendar_end
    name = 'Merged '
    i = 0
    while True:
        calendar = db(db.calendar.name == name).select().first()
        if calendar is not None:
            i=int(i+1)
            name = 'Merged '
            name = name + str(i)
        else:
            break

    db.calendar.insert(name= name, start_date=earliest_start, end_date=latest_end)
    cid = (db(db.calendar.name == name).select()).first().id


    #logger.info(merged_events)
    for c in calendar_ids:
        event_list = db(db.calendar_event.calendar == c).select()
        if event_list is not None:
            for e in event_list:
                db.calendar_event.insert(calendar = cid, title=e.title,author=e.author, date_occur=e.date_occur,description=e.description,recur=e.recur, recurring=e.recurring,start_time=e.start_time,end_time=e.end_time)
                logger.info(e.title)
                #merged_events.insert(title=e.title,author=e.author)
                #logger.info(e.created_on)
        #db.calendar(c).delete()
    merged_events=db(db.calendar_event.calendar==cid).select().first()
    logger.info(merged_events)
    return "okay"

@auth.requires_login()
def add_event():
    y = int(request.args(1))
    m = request.args(2)
    d = int(request.args(3))
    month_names = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
    month = month_names.index(m)+1
    year = y
    day = d
    month_days = [31, 28, 31, 30, 31, 30, 31, 30, 31, 31, 30, 31]
    if (y%4 == 0):
        month_days[1] = 29
    else:
        month_days[1] = 28
    if(month_days[month-1] < d):
        day = d - month_days[month-1]
        month +=1
    if (month > 12):
        year +=1
        if (y%4 == 0):
            month_days[1] = 29
        else:
            month_days[1] = 28
    date_occur = date(year,month,day)
    start_date = (db.calendar(request.args(0))).start_date
    end_date = (db.calendar(request.args(0))).end_date
    logger.info(date_occur);
    logger.info(start_date);
    if((date_occur <= end_date) & (date_occur >= start_date)):
        logger.info(date_occur);
        form = SQLFORM(db.calendar_event, fields=['title', 'start_time', 'end_time', 'description', 'recur'])
        form.vars.calendar = db.calendar(request.args(0))
        form.vars.date_occur = date_occur
        if form.process().accepted:
            relatedevent = db.calendar_event(form.vars.id)
            if (relatedevent.recur == True):
                relatedevent.update_record(recurring=True)
            logger.info(relatedevent.recurring)
            redirect(URL('default', 'month', args=[request.args(0)]))
    else:
        session.flash = T("You can't add events out of range")
        redirect(URL('default', 'month', args=[request.args(0)]))
    return dict(form = form)

@auth.requires_login()
def delete_event():
    calendar_event = db.calendar_event(request.args(1))
    if (int(calendar_event.author) == int(auth.user_id)):
        db(db.calendar_event.id == calendar_event).delete()
        redirect(URL('default', 'month', args=[request.args(0)]))
    else:
        session.flash = T("You can't delete another user's calendar")
        redirect(URL('default', 'index'))
    return dict()


@auth.requires_login()
def delete_calendar():
    calendar = db.calendar(request.args(0))
    if (int(calendar.author) == int(auth.user_id)):
        db(db.calendar.id == calendar).delete()
        db(db.shared_with.calendar == request.args(0)).delete()
        redirect(URL('default', 'index'))
    else:
        session.flash = T("You can't delete another user's calendar")
        redirect(URL('default', 'index'))
    return dict()


@auth.requires_login()
def remove_share():
    db(db.shared_with.calendar == request.args(0)).delete()
    redirect(URL('default', 'shared'))
    return dict()


@auth.requires_login()
def edit_calendar():
    calendar = db.calendar(request.args(0))
    #This if block prevents the user from editing a nonexistant post
    if calendar is None:
        session.flash = T('No such calendar')
        redirect(URL('default', 'index'))
    #This if-block prevents other people other than the owner of a post from editing the post
    if (int(calendar.author) == int(auth.user_id)):
        form = SQLFORM(db.calendar, record=calendar)
    else:
        session.flash = T("You can't edit another user's calendar")
        redirect(URL('default', 'index'))
    if form.process().accepted:
        session.flash = T('The calendar was edited')
        redirect(URL('default', 'index'))
    return dict(form=form)

@auth.requires_login()
def edit_event():
    calendar_event = db.calendar_event(request.args(1))
    #This if block prevents the user from editing a nonexistant post
    if calendar_event is None:
        session.flash = T('No such event')
        redirect(URL('default', 'month', args=[request.args(0)]))
    #This if-block prevents other people other than the owner of a post from editing the post
    if (int(calendar_event.author) == int(auth.user_id)):
        form=SQLFORM(db.calendar_event,record=calendar_event)
        #form = SQLFORM(db.calendar_event, record=calendar_event, fields=['title', 'date_occur', 'start_time', 'end_time', 'description', 'attachment1', 'attachment2', 'attachment3', 'recur_sunday', 'recur_monday', 'recur_tuesday', 'recur_wednesday', 'recur_thursday', 'recur_friday', 'recur'])
    else:
        session.flash = T("You can't edit another user's event")
        redirect(URL('default', 'index'))
    #form.vars.calendar = db.calendar(request.args(0))
    if form.process().accepted:
        relatedevent = db.calendar_event(request.args(1))
        if (relatedevent.recur == True):
            db(db.calendar_event.id==request.args(1)).update(recurring=True)
        session.flash = T('The event was edited')
        redirect(URL('default', 'month', args=[request.args(0)]))
    return dict(form=form)

@auth.requires_login()
def share_calendar():
    user = (db(db.auth_user.email == request.vars.email).select())
    id=0
    for u in user:
        id = u.id
    cal = int(request.vars.calendar)
    db.shared_with.insert(shared_user= id, calendar=cal, edit_permission=True)
    db(db.calendar.id==cal).update(shared_boolean=True)
    return dict()


@auth.requires_login()
def privatize():
    cal = int(request.args(0))
    db(db.shared_with.calendar == cal).delete()
    db(db.calendar.id==cal).update(shared_boolean=False)
    redirect('default', 'shared')
    return dict()


@auth.requires_login()
def shared():
    calendar_list = db((db.calendar.author == auth.user_id) & (db.calendar.shared_boolean == True)).select()
    shared_list = db(db.shared_with.shared_user == auth.user_id).select()
    return dict(calendar_list = calendar_list, shared_list=shared_list)


@auth.requires_login()
def month():
    relatedcalendar = db.calendar(request.args(0))
    calendar_name=relatedcalendar.name
    onetimeevents = db(db.calendar_event.calendar==relatedcalendar).select()
    return dict(onetimeevents = onetimeevents,calendar_name=calendar_name)


@auth.requires_login()
def shared_month():
    relatedcalendar = db.calendar(request.args(0))
    onetimeevents = db(db.calendar_event.calendar==relatedcalendar).select()
    return dict(onetimeevents = onetimeevents)

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    http://..../[app]/default/user/bulk_register
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()

#reset everything
@auth.requires_login()
def reset():
    db(db.calendar.id>0).delete()
    db(db.calendar_event.id>0).delete()
    db(db.shared_with.id>0).delete()
    db.calendar.truncate()
    db.calendar_event.truncate()
    db.shared_with.truncate()
    redirect(URL('default','index'))

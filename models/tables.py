#########################################################################
## Define your tables below; for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

from datetime import datetime

db.define_table('calendar',
             Field('author', db.auth_user, default=auth.user_id),
             Field('created_on', 'datetime', default=datetime.utcnow()),
             Field('name'),
             Field('start_date', 'date'),
             Field('end_date', 'date'),
             Field('shared_boolean', 'boolean', default=False),
             Field('is_favorite', 'boolean', default=False)
            )

db.define_table('calendar_event',
             Field('author', db.auth_user, default=auth.user_id),
             Field('created_on', 'datetime', default=datetime.utcnow()),
             Field('date_occur', 'date'), #mandatory
             Field('start_time', 'time'), #mandatory
             Field('end_time', 'time'), #mandatory
             Field('title'), #mandatory
             Field('description', 'text'),
             Field('recurring', 'boolean', default=False),
             Field('recur', 'boolean'),
             Field('calendar', 'reference calendar')
            )

# to get all the users a calendar is shared with
db.define_table('shared_with',
             Field('shared_user', db.auth_user),
             Field('calendar', 'reference calendar'),
             Field('edit_permission', 'boolean', default=False),
            )

db.calendar.id.readable = db.calendar.id.writable = False
db.calendar.author.readable = db.calendar.author.writable = False
db.calendar.created_on.readable=db.calendar.created_on.writable = False
db.calendar.shared_boolean.readable=db.calendar.shared_boolean.writable = False

db.calendar_event.id.readable = db.calendar_event.id.writable = False
db.calendar_event.author.readable = db.calendar_event.author.writable = False
db.calendar_event.created_on.readable = db.calendar_event.created_on.writable = False
db.calendar_event.recurring.readable = db.calendar_event.recurring.writable = False
db.calendar_event.calendar.readable = db.calendar_event.calendar.writable = False
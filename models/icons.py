#This generates html tags for the corresponding icons
#These icon will only appear if the user created the post
icon_edit = I(_class = 'fa fa-pencil-square-o')
icon_delete = I(_class = 'fa fa-times')
icon_select = I(_class = 'fa fa-circle-o')